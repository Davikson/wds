/*
 * LSM303DLHC.c
 *
 *  Created on: May 2, 2017
 *      Author: davik
 */


#include "i2c.h"
#include "stdint.h"
#include "LSM303DLHC.h"

void LSM303_ACC_Init()
{
	uint8_t REG1_Settings = LSM303_ACC_Axis_ZYX_ENABLE
			| LSM303_ACC_LowPowerMode_Disable
			| LSM303_ACC_DataRate_100Hz;

	uint8_t REG4_Settings = LSM303_ACC_ContinousUpdate
			| LSM303_ACC_Endianness_LSB
			| LSM303_ACC_Scale_4g
			| LSM303_ACC_HiResOutput_Disable;

	HAL_I2C_Mem_Write(&hi2c1, LSM303_ACC_DevAddress, LSM303_ACC_CTRL_REG1_A, 1, &REG1_Settings, 1, 100);
	HAL_I2C_Mem_Write(&hi2c1, LSM303_ACC_DevAddress, LSM303_ACC_CTRL_REG4_A, 1, &REG4_Settings, 1, 100);
}

void LSM303_ACC_Read(float * pfData)
{
	uint8_t RawData[6];
	int16_t Data[3];
	uint8_t params;

	HAL_I2C_Mem_Read(&hi2c1, LSM303_ACC_DevAddress, LSM303_ACC_OUT_REG | 0x80, 1, RawData, 6, 100);
	HAL_I2C_Mem_Read(&hi2c1, LSM303_ACC_DevAddress, LSM303_ACC_CTRL_REG4_A, 1, &params, 1, 100);

	int i;
	if(!(params & 0x40))// check endiannes
	{
		for(i = 0; i < 3; i++)
		{
			Data[i] = ((int16_t)RawData[2*i+1] << 8) | (int16_t)RawData[2*i];
		}
	}
	else
	{
		for(i = 0; i < 3; i++)
		{
			Data[i] = (((int16_t)RawData[2*i] << 8) | (int16_t)RawData[2*i+1]);
		}
	}

	switch(params & 0x30)// check scale
	{
	case LSM303_ACC_Scale_2g:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*2)/(float)INT16_MAX;
		break;
	case LSM303_ACC_Scale_4g:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*4)/(float)INT16_MAX;
		break;
	case LSM303_ACC_Scale_8g:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*8)/(float)INT16_MAX;
		break;
	case LSM303_ACC_Scale_16g:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*16)/(float)INT16_MAX;
		break;
	}
}

void LSM303_MAG_Init()
{
	uint8_t CRA_Settings = LSM303_MAG_DataRate_30;
	uint8_t CRB_Settings = LSM303_MAG_InputRange_4_7;
	uint8_t MR_Settings = LSM303_MAG_OperatingMode_Continous;

	HAL_I2C_Mem_Write(&hi2c1, LSM303_MAG_DevAddress, LSM303_MAG_CRA_REG_M, 1, &CRA_Settings, 1, 100);
	HAL_I2C_Mem_Write(&hi2c1, LSM303_MAG_DevAddress, LSM303_MAG_CRB_REG_M, 1, &CRB_Settings, 1, 100);
	HAL_I2C_Mem_Write(&hi2c1, LSM303_MAG_DevAddress, LSM303_MAG_MR_REG_M, 1, &MR_Settings, 1, 100);
}

void LSM303_MAG_Read(float * pfData)
{
	uint8_t RawData[6];
	int16_t Data[3];
	uint8_t params;

	HAL_I2C_Mem_Read(&hi2c1, LSM303_MAG_DevAddress, LSM303_MAG_OUT_REG | 0x80, 1, RawData, 6, 100);
	HAL_I2C_Mem_Read(&hi2c1, LSM303_MAG_DevAddress, LSM303_MAG_CRB_REG_M, 1, &params, 1, 100);

	int i;
	for(i = 0; i < 3; i++)
	{
		Data[i] = (int16_t)((RawData[2*i] << 8) | RawData[2*i+1]);
	}
	switch(params & 0xE0)// check scale
	{
	case LSM303_MAG_InputRange_1_3:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*1.3f)/(float)INT16_MAX;
		break;
	case LSM303_MAG_InputRange_1_9:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*1.9f)/(float)INT16_MAX;
		break;
	case LSM303_MAG_InputRange_2_5:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*2.5f)/(float)INT16_MAX;
		break;
	case LSM303_MAG_InputRange_4_0:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*4.0f)/(float)INT16_MAX;
		break;
	case LSM303_MAG_InputRange_4_7:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*4.7f)/(float)INT16_MAX;
		break;
	case LSM303_MAG_InputRange_5_6:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*5.6f)/(float)INT16_MAX;
		break;
	case LSM303_MAG_InputRange_9_1:
		for(i = 0; i < 3; i++) pfData[i] = ((float)Data[i]*9.1f)/(float)INT16_MAX;
		break;
	}
	switch(params & 0xE0)// check scale
	{
	case LSM303_MAG_InputRange_1_3:
		pfData[0] = Data[0] /(float)1100;
		pfData[1] = Data[1] /(float)855;
		pfData[2] = Data[2] /(float)1100;
		break;
	case LSM303_MAG_InputRange_1_9:
		pfData[0] = (float)Data[0]/(float)855;
		pfData[1] = (float)Data[1]/(float)760;
		pfData[2] = (float)Data[2]/(float)855;
		pfData[0] = Data[0] /(float)1100;
		pfData[1] = Data[1] /(float)855;
		pfData[2] = Data[2] /(float)1100;
		break;
	case LSM303_MAG_InputRange_2_5:
		pfData[0] = (float)Data[0]/(float)670;
		pfData[1] = (float)Data[1]/(float)600;
		pfData[2] = (float)Data[2]/(float)670;
		break;
	case LSM303_MAG_InputRange_4_0:
		pfData[0] = (float)Data[0]/(float)450;
		pfData[1] = (float)Data[1]/(float)400;
		pfData[2] = (float)Data[2]/(float)450;
		break;
	case LSM303_MAG_InputRange_4_7:
		pfData[0] = (float)Data[0]/(float)400;
		pfData[1] = (float)Data[1]/(float)355;
		pfData[2] = (float)Data[2]/(float)400;
		break;
	case LSM303_MAG_InputRange_5_6:
		pfData[0] = (float)Data[0]/(float)330;
		pfData[1] = (float)Data[1]/(float)295;
		pfData[2] = (float)Data[2]/(float)330;
		break;
	case LSM303_MAG_InputRange_9_1:
		pfData[0] = (float)Data[0]/(float)230;
		pfData[1] = (float)Data[1]/(float)205;
		pfData[2] = (float)Data[2]/(float)230;
		break;
	}
}
