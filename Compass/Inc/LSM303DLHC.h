/*
 * LSM303DLHC.h
 *
 *  Created on: May 2, 2017
 *      Author: davik
 */

#ifndef LSM303DLHC_H_
#define LSM303DLHC_H_

//LSM303 Accelerometer defines
//LSM303 Accelerometer address
#define LSM303_ACC_DevAddress (0x19 << 1) // 0011 001x

//Output register
#define LSM303_ACC_OUT_REG	0x28	//[OUT_X_L_A][OUT_X_H_A][OUT_Y_L_A][OUT_Y_H_A][OUT_Z_L_A][OUT_Z_H_A]

// CTRL_REG1_A = [ODR3][ODR2][ODR1][ODR0][LPEN][ZEN][YEN][XEN]
#define LSM303_ACC_CTRL_REG1_A	0x20
//[ODR3:0] Data rate configuration
#define LSM303_ACC_DataRate_PowerDownMode	0x00	// 0000 0000 // default
#define LSM303_ACC_DataRate_1Hz		0x10	// 0001 0000
#define LSM303_ACC_DataRate_10Hz	0x20	// 0010 0000
#define LSM303_ACC_DataRate_25Hz	0x30	// 0011 0000
#define LSM303_ACC_DataRate_50Hz	0x40	// 0100 0000
#define LSM303_ACC_DataRate_100Hz	0x50	// 0101 0000
#define LSM303_ACC_DataRate_200Hz	0x60	// 0110 0000
#define LSM303_ACC_DataRate_400Hz	0x70	// 0111 0000
// [LPEN] Low-power mode
#define LSM303_ACC_LowPowerMode_Disable		0x00	// 0000 0000 // default
#define LSM303_ACC_LowPowerMode_Enable		0x08	// 0000 1000
// [ZEN][YEN][XEN] Axis configuration
#define LSM303_ACC_Axis_Z_ENABLE	0x04	// 0000 0100
#define LSM303_ACC_Axis_Y_ENABLE	0x02	// 0000 0010
#define LSM303_ACC_Axis_X_ENABLE	0x01	// 0000 0001
#define LSM303_ACC_Axis_ZYX_ENABLE	0x07	// 0000 0111 // default

// CTRL_REG4_A = [BDU][BLE][FS1][FS0][HR][0][0][SIM]
#define LSM303_ACC_CTRL_REG4_A 0x23
// [BDU] Block data update
#define LSM303_ACC_ContinousUpdate	0x00	// default
#define LSM303_ACC_OutputNotUpdatedUntilRead	0x80	// 1000 0000
// [BLE] Big/Little endian
#define LSM303_ACC_Endianness_LSB	0x00	// default
#define LSM303_ACC_Endianness_MSB	0x40
// [FS1:0] Full-scale selection
#define LSM303_ACC_Scale_2g		0x00	// 0000 0000 // default
#define LSM303_ACC_Scale_4g		0x10	// 0001 0000
#define LSM303_ACC_Scale_8g		0x20	// 0010 0000
#define LSM303_ACC_Scale_16g	0x30	// 0011 0000
// [HR] High-resolution output mode
#define LSM303_ACC_HiResOutput_Disable	0x00	// default
#define LSM303_ACC_HiREsOutput_Enable	0x08
// [SIM] SPI serial interface mode selection
#define LSM303_ACC_SPI4wire	0x00	// default
#define LSM303_ACC_SPI3wire 0x01


// LSM303 Magnetic field defines
// LSM303 Magnetic field adress
#define LSM303_MAG_DevAddress	(0x1E << 1) // 0011110x

// Output Register
#define LSM303_MAG_OUT_REG	0x03		//[OUT_X_L_M][OUT_X_H_M][OUT_Y_L_M][OUT_Y_H_M][OUT_Z_L_M][OUT_Z_H_M]

// CRA_REG_M = [TEMP_EN][0][0]DO2][DO1][DO0][0][0]
#define LSM303_MAG_CRA_REG_M	0x00
// [TMPEN] Temperature sensor
#define LSM303_MAG_TEMP_Disable	0x00	// 0000 0000 //default
#define LSM303_MAG_TEMP_Enable	0x80	// 1000 0000
// [DO2:0] Data rate configuration
#define LSM303_MAG_DataRate_0_75	0x00
#define LSM303_MAG_DataRate_1_5		0x04
#define LSM303_MAG_DataRate_3		0x08
#define LSM303_MAG_DataRate_7_5		0x0C
#define LSM303_MAG_DataRate_15		0x10
#define LSM303_MAG_DataRate_30		0x14
#define LSM303_MAG_DataRate_75		0x18
#define LSM303_MAG_DataRate_220		0x1C

// CRB_REG_M = [GN2][GN1][GN0][0][0][0][0][0]
#define LSM303_MAG_CRB_REG_M	0x01
// [GN2:0]
#define LSM303_MAG_InputRange_1_3	0x20
#define LSM303_MAG_InputRange_1_9	0x40
#define LSM303_MAG_InputRange_2_5	0x60
#define LSM303_MAG_InputRange_4_0	0x80
#define LSM303_MAG_InputRange_4_7	0xA0
#define LSM303_MAG_InputRange_5_6	0xC0
#define LSM303_MAG_InputRange_9_1	0xE0

// MR_REG_M = [0][0][0][0][0][0][MD1][MD2]
#define LSM303_MAG_MR_REG_M	0x02
// [MD1:0] Operating mode
#define LSM303_MAG_OperatingMode_Continous	0x00
#define LSM303_MAG_OperatingMode_Single		0x01
#define LSM303_MAG_OperatingMode_Sleep		0x03

// TODO
/*
struct LSM303_ACC_Init_Struct
{
	uint8_t CTRL_REG1 = 0x08;
	uint8_t CTRL_REG2 = 0x00;
	uint8_t CTRL_REG3 = 0x00;
	uint8_t CTRL_REG4 = 0x00;
	uint8_t CTRL_REG5 = 0x00;
};*/

void LSM303_ACC_Init();
void LSM303_ACC_Read(float * pfData);

void LSM303_MAG_Init();
void LSM303_MAG_Read(float * pfData);

#endif /* LSM303DLHC_H_ */
