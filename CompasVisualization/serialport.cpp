#include "stdio.h"
#include "string.h"
#include "sys/ioctl.h"
#include <unistd.h>
#include "fcntl.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "limits.h"
#include "sys/file.h"
#include "errno.h"
#include "serialport.h"

SerialPort::SerialPort() : connected(false)
{

}

SerialPort::SerialPort(const char * device, int baudrate, const char * mode) : connected(false)
{
    Connect(device, baudrate, mode);
}

SerialPort::~SerialPort()
{
    Disconnect();
}

int SerialPort::Connect(const char * _device, int _baudrate, const char * _mode)
{
    int baudrate = B9600;
    switch (_baudrate)
    {
    case      50:   baudrate =      B50; break;
    case      75:   baudrate =      B75; break;
    case     110:   baudrate =     B110; break;
    case     134:   baudrate =     B134; break;
    case     150:   baudrate =     B150; break;
    case     200:   baudrate =     B200; break;
    case     300:   baudrate =     B300; break;
    case     600:   baudrate =     B600; break;
    case    1200:   baudrate =    B1200; break;
    case    1800:   baudrate =    B1800; break;
    case    2400:   baudrate =    B2400; break;
    case    4800:   baudrate =    B4800; break;
    case    9600:   baudrate =    B9600; break;
    case   19200:   baudrate =   B19200; break;
    case   38400:   baudrate =   B38400; break;
    case   57600:   baudrate =   B57600; break;
    case  115200:   baudrate =  B115200; break;
    case  230400:   baudrate =  B230400; break;
    case  460800:   baudrate =  B460800; break;
    case  500000:   baudrate =  B500000; break;
    case  576000:   baudrate =  B576000; break;
    case  921600:   baudrate =  B921600; break;
    case 1000000:   baudrate = B1000000; break;
    case 1152000:   baudrate = B1152000; break;
    case 1500000:   baudrate = B1500000; break;
    case 2000000:   baudrate = B2000000; break;
    case 2500000:   baudrate = B2500000; break;
    case 3000000:   baudrate = B3000000; break;
    case 3500000:   baudrate = B3500000; break;
    case 4000000:   baudrate = B4000000; break;
    default: printf("Invalid baudrate: %d.\nBaudrate 9600 set.\n", _baudrate);
        break;
    }

    int cbits = CS8, cpar = 0, ipar = IGNPAR, bstop = 0;
    switch (_mode[0])
    {
    case '8':   cbits = CS8; break;
    case '7':   cbits = CS7; break;
    case '6':   cbits = CS6; break;
    case '5':   cbits = CS5; break;
    default: printf("Invalid number of data bits '%c'.\n8 data bits set.\n", _mode[0]); break;
    }
    switch (_mode[1])   // parity
    {
    case 'n': case 'N': cpar = 0;                   ipar = IGNPAR;  break;  // no parity
    case 'e': case 'E': cpar = PARENB;              ipar = INPCK;   break;  // even parity
    case 'o': case 'O': cpar = (PARENB | PARODD);   ipar = INPCK;   break;  // odd parity
    default: printf("Invalid parity '%c'.\nNo parity set.", _mode[1]); break;
    }
    switch (_mode[2])   // stop bits
    {
    case '1':   bstop = 0;      break;  // one stop bit
    case '2':   bstop = CSTOPB; break;  // two stop bits
    default: printf("Invalid number of stop bits '%c',\n0 stop bits set.", _mode[2]); break;
    }

    cport = open(_device, O_RDWR | O_NOCTTY | O_NDELAY);
    if (cport == -1)
    {
        perror("Unable to open ComPort.\n");
        return -1;
    }

    if (flock(cport, LOCK_EX | LOCK_NB) == -1)
    {
        close(cport);
        perror("Another proces has locked the ComPort.\n");
        return -2;
    }

    if (tcgetattr(cport, &old_port_setings) == -1)
    {
        close(cport);
        flock(cport, LOCK_UN);
        perror("Unable to read portsettings.\n");
        return -3;
    }

    memset(&new_port_settings, 0, sizeof(new_port_settings));
    new_port_settings.c_iflag = ipar;
    new_port_settings.c_oflag = 0;
    new_port_settings.c_cflag = cbits | cpar | bstop | CLOCAL | CREAD;
    new_port_settings.c_lflag = 0;
    new_port_settings.c_cc[VMIN] = 0;
    new_port_settings.c_cc[VTIME] = 0;

    cfsetispeed(&new_port_settings, baudrate);
    cfsetospeed(&new_port_settings, baudrate);

    if(tcsetattr(cport, TCSANOW, &new_port_settings) == -1)
    {
        tcsetattr(cport, TCSANOW, &old_port_setings);
        close(cport);
        flock(cport, LOCK_UN);

        perror("Unable to adjust port settings.\n");
        return -4;
    }

    int status;
    if(ioctl(cport, TIOCMGET, &new_port_settings) == -1)
    {
        tcsetattr(cport, TCSANOW, &old_port_setings);
        close(cport);
        flock(cport, LOCK_UN);

        perror("Unable to get port status.\n");
        return -5;
    }

    status |= TIOCM_DTR;
    status |= TIOCM_RTS;

    if(ioctl(cport, TIOCMSET, &status) == -1)
    {
        tcsetattr(cport, TCSANOW, &old_port_setings);
        close(cport);
        flock(cport, LOCK_UN);

        perror("Unable to set port status");
        return -6;
    }

    connected = true;
    return 0;
}

void SerialPort::Disconnect()
{
    int status;
    if(ioctl(cport, TIOCMGET, &status) == -1)
    {
        perror("Unable to get port status.\n");
    }

    status &= ~TIOCM_DTR;
    status &= ~TIOCM_RTS;

    if(ioctl(cport, TIOCMSET, &status) == -1)
    {
        perror("Unable to set port status.\n");
    }

    tcsetattr(cport, TCSANOW, &old_port_setings);
    close(cport);
    flock(cport, LOCK_UN);

    connected = false;
}

int SerialPort::GetArray(unsigned char * buffer, int len)
{
    if(connected)
    {
        int n = read(cport, buffer, len);
        return n;
    }
    else
        return -1;
}

int SerialPort::SendArray(unsigned char * buffer, int len)
{
    if(connected)
    {
        int n = write(cport, buffer, len);
        return n;
    }
    else
        return -1;
}
