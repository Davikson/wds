#include <QApplication>
#include <iostream>
#include <boost/format.hpp>
#include <thread>
#include "types.h"
#include "serialport.h"
#include "mainwindow.h"

void SerialPortThread(const char * portName, datatype_t * data);

int main(int argc, char *argv[])
{
    datatype_t data[6] = {0, 0, 0, 0, 0, 0};
    std::thread serialPortThread(ComPortThread, argv[1], data);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

SerialPortThread(const char * portName, datatype_t * data)
{
    data_t myData;

    char mode[] = "8N1\0";
    SerialPort comPort(portName, 9600, mode);
    int nbytes;

    while (true)
    {
        nbytes = comPort.GetArray(myData.buffer, sizeof(myData));
        if (nbytes > 0)
        {
            if (myData.flag == 'X')
            {
                datatype_t checksum = 0;
                for (int i = 0; i < 6; i++) checksum += myData.data[i];
                if (abs(checksum - myData.checksum) < 1E-7)
                {
                    memcpy(data, myData.data, 6 * sizeof(datatype_t));
                    for (int i = 0; i < 6; i++)
                        std::cout << boost::format("%1% ") % myData.data[i];
                }
                else
                    std::cout << boost::format("Data corrupted: %1% != %2%") % myData.checksum % checksum;
                std::cout << std::endl;
            }
        }
    }
}
