#ifndef TYPES_H
#define TYPES_H


typedef float datatype_t;

typedef union
{
  struct
  {
      uint8_t flag;
      datatype_t ACC_Data[3];
      datatype_t MAG_Data[3];
      datatype_t checksum;
  };
  struct
  {
      uint8_t f;
      datatype_t data[7];
  };
  uint8_t buffer[1 + 7 * sizeof(datatype_t)];
} data_t;

#endif // TYPES_H
