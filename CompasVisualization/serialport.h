#ifndef SERIALPORT_H
#define SERIALPORT_H
#include "termios.h"

class SerialPort
{
private:
    int cport;
    bool connected;
    struct termios new_port_settings, old_port_setings;
public:
    SerialPort();
    SerialPort(const char * device, int baudrate = 9600, const char * mode = "8N1");
    ~SerialPort();

    int Connect(const char * device, int baudrate, const char * mode);
    void Disconnect();

    int SendArray(unsigned char * buffer, int len);
    int GetArray(unsigned char * buffer, int len);
};

#endif // SERIALPORT_H
